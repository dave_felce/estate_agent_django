from django.conf.urls import url

from .views import Sale

app_name = 'sale'

urlpatterns = [
    url(r'^$', Sale.as_view(), name='index'),
]