from django.test import TestCase, Client
from model_mommy import mommy
from unittest import skip

class SaleTests(TestCase):
    """ Test Sale """
    def test_for_sale_search(self):
        """ Sale search page

        TODO:
            This needs fleshing out, a lot!

        """
        response = self.client.get('/sale/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<title>For Sale</title>")

    @skip("SaleTests needs re-writing!")
    def setUp(self):
        self.client = Client()
        user = mommy.make('User', username='HOME_TEST_PERSON')
        person = mommy.make('people.Details', user=user)
        mommy.make('places.Property', user=person)

    @skip("SaleTests needs re-writing!")
    def test_index(self):
        response = self.client.get('/')

        self.assertEqual(response.status_code, 200)

        # pprint(vars(response))
        self.assertContains(response, '<title>EstateAgent</title>')
        self.assertContains(response, '<h3 class="page-header global-header">Latest Properties</h3>')

