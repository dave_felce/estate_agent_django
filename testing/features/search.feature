Feature: Performing a search

  Scenario: Search for a red house
    Given I am on the search screen
     When I search for a property with the description "red" and a location "London"
     Then I see a list of properties appear
      And I see the search term "red" in the description of the property
      And I see the location is "London" on the property
