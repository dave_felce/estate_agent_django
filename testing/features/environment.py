from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from elementium.drivers.se import SeElements

def before_all(context):

    context.browser = SeElements(webdriver.Remote(
        desired_capabilities=DesiredCapabilities.CHROME,
        command_executor="http://hub:4444/wd/hub"
    ))

def after_all(context):
    pass
    #context.browser.quit()