from behave import given, then, when
from behave_django.decorators import fixtures



@fixtures('initial.json')
@given(u'I am on the search screen')
def step_impl(context):
    context.browser.navigate('http://webapp:8000')

@when(u'I search for a property with the description "{description}" and a location "{area}"')
def step_impl(context, description, area):
    context.browser.find('#area').write(f'{area}')
    context.browser.find('#description').write(f'{description}')
    context.browser.find('button').click()

@then(u'I see a list of properties appear')
def step_impl(context):
    pass

@then(u'I see the search term "{term}" in the description of the property')
def step_impl(context, term):
    pass
    #context.browser.find(term, wait=True)

@then(u'I see the location is "London" on the property')
def step_impl(context):
    pass

