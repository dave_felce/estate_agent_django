from django.conf.urls import url

from . import views

app_name = 'articles'

urlpatterns = [
    url(r'^article/(?P<article_id>[0-9]+)/$', views.ArticleDetail.as_view(), name='article_detail'),
]