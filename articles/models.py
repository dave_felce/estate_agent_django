from django.db import models
from django.utils import timezone

class Article(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    short_description = models.CharField(max_length=50)
    content = models.TextField(max_length=5000)
    created = models.DateTimeField(editable=False)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = timezone.now()
        self.updated = timezone.now()
        return super(Article, self).save(*args, **kwargs)

    class Meta:
        managed = True
        db_table = 'article'
