from django.views import View

from .models import Article

from django.shortcuts import render, get_object_or_404

# Create your views here.
class ArticleDetail(View):
    """Detail for a single Article
    """

    def get(self, request, article_id):
        """ get HTTP method """

        article = get_object_or_404(Article, pk=article_id)
        context = {
            # TODO: Proper page title
            'page_title': 'Article',
            'article': article,
        }
        return render(request, 'articles/article_detail.html', context)
