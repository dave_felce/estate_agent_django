from django.urls import reverse
from django.test import TransactionTestCase
from model_mommy import mommy

class PeopleTests(TransactionTestCase):

    def setUp(self):

        self.user = mommy.make('Details').user

    def test_agent_detail_context(self):

        self.client.force_login(self.user)

        url = reverse('people:agent_detail', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.username, response.context['person'].user.username)

    def test_agent_detail_template(self):

        self.client.force_login(self.user)

        url = reverse('people:agent_detail', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Agent detail</title>')

    def test_owner_detail_context(self):

        self.client.force_login(self.user)

        url = reverse('people:owner_detail', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.username, response.context['person'].user.username)

    def test_owner_detail_template(self):

        self.client.force_login(self.user)

        url = reverse('people:owner_detail', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Owner detail</title>')
        self.assertContains(response, f'<td>{self.user.details.address}</td>')
        self.assertContains(response, f'<td>{self.user.details.town_or_city}</td>')
        self.assertContains(response, f'<td>{self.user.details.postcode}</td>')

    def test_tenant_detail_context(self):

        url = reverse('people:tenant_detail', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.username, response.context['person'].user.username)

    def test_tenant_detail_template(self):

        url = reverse('people:tenant_detail', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Tenant detail</title>')

    def test_owner_property_list_context(self):

        place = mommy.make('places.Property', user=self.user.details)

        url = reverse('people:owner_property_list', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.username, response.context['person'].user.username)

    def test_owner_property_list_template(self):

        place = mommy.make('places.Property', user=self.user.details)

        url = reverse('people:owner_property_list', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Owner properties</title>')
        self.assertContains(response, '<div class="page-content">')
        self.assertContains(response, f'{place.description}')

    def test_tenant_property_list_context(self):

        url = reverse('people:tenant_property_list', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.username, response.context['person'].user.username)

    def test_tenant_property_list_template(self):

        url = reverse('people:tenant_property_list', kwargs={'user_id': self.user.details.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Tenant properties</title>')
