from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.views import View
from django.contrib.staticfiles.templatetags.staticfiles import static

# from django.contrib.auth.models import User
from .models import Details
from .forms import RegisterForm

# TODO: Remember a lot of these templates were copies of the original detail.html and login.html
# login.html was usually a list of objects to display to became property_list.html, where detail.html got
# copied into the three people template dirs.  They need sorting out, and were left mostly as they
# were to get the tests working

# Display details of a single agent, so person obj here will be of type 'agent'
def agent_detail(request, user_id):
    """Detail page for a single agent (Person obj)

    Args:
        user_id: id to retrieve Person obj
    """
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/agent/detail.html', {'person': person, 'page_title': 'Agent detail'})

# Display details of a single owner, so person obj here will be of type 'owner'
@login_required
def owner_detail(request, user_id):
    """Detail page for a single property owner (Person obj)

    Args:
        user_id: id to retrieve Person obj
    """
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/owner/detail.html', {'person': person, 'page_title': 'Owner detail'})

@login_required
def my_profile(request):
    """ Profile page for the current user - linked to from right navbar name link """

    user_id = request.user.details.id  # id of people.Details rather than Django user
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/my_profile/index.html', {'person': person, 'page_title': 'My profile'})

@login_required
def my_preferences(request):
    """ Preferences page for the current user - linked to from profile page """

    user_id = request.user.details.id  # id of people.Details rather than Django user
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/my_profile/preferences.html', {'person': person, 'page_title': 'My preferences'})

# Display details of a single tenant, so person obj here will be of type 'tenant'
def tenant_detail(request, user_id):
    """Detail page for a single tenant (Person obj)

    Args:
        user_id: id to retrieve Person obj
    """
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/tenant/detail.html', {'person': person, 'page_title': 'Tenant detail'})

def owner_property_list(request, user_id):
    """List of properties belonging to owner (Person obj)

    Args:
        user_id: id to retrieve Person obj

    TODO:
        Maybe this will be 'display properties owned
        by me' or something, in which case user_id would be redundant and the user_id of the
        logged in user used. The details.html already lists property owned/rented by the person

    """
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/owner/property_list.html', {'person': person, 'page_title': 'Owner properties'})

def tenant_property_list(request, user_id):
    """Listing page for properties (having been) rented by tenant (Person obj)

    Args:
        user_id: id to retrieve Person obj

    TODO:
        Clearly delineated 'current' property at top, then historic list?
        This probably needs a separate method for 'display my rented properties'

    """
    person = get_object_or_404(Details, pk=user_id)
    return render(request, 'people/tenant/property_list.html', {'person': person, 'page_title': 'Tenant properties'})

class Register(View):
    def get(self, request):
        """get http method: search form will be empty
        """
        form = RegisterForm()
        return render(request, 'people/register.html', {'form': form})

    def post(self, request):
        """post http method: registration form will be submitted
        """

        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.details.address = form.cleaned_data.get('address')
            user.details.town_or_city = form.cleaned_data.get('town_or_city')
            user.details.county = form.cleaned_data.get('county')
            user.details.postcode = form.cleaned_data.get('postcode')
            user.details.number = form.cleaned_data.get('number')
            user.details.email = form.cleaned_data.get('email')
            user.details.rooms = form.cleaned_data.get('rooms')
            user.details.floors = form.cleaned_data.get('floors')
            user.details.bathrooms = form.cleaned_data.get('bathrooms')
            user.details.bedrooms = form.cleaned_data.get('bedrooms')
            user.details.email_alerts = form.cleaned_data.get('email_alerts')
            # All users registering via the site are 'public' but can be manually
            # changed later via the admin interface, e.g. into a 'tenant'
            user.details.user_type = 'public'
            user.details.image_original = 'people/original/anonymous_user.png'
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home:index')
        else:
            return render(request, 'people/register.html', {'form': form})

