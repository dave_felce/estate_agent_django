from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Details(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    address = models.TextField(max_length=300)
    town_or_city = models.CharField(max_length=100)
    county = models.CharField(max_length=100)
    postcode = models.CharField(max_length=10)
    number = models.CharField(max_length=30)
    email = models.EmailField()
    email_alerts = models.BooleanField(default=False)
    user_type = models.CharField(max_length=100,
                                 choices=[('public', 'Public'),
                                          ('agent', 'Agent'),
                                          ('tenant', 'Tenant'),
                                          ('owner', 'Owner')]
                                )

    rooms = models.IntegerField(default=0)
    floors = models.IntegerField(default=0)
    bathrooms = models.IntegerField(default=0)
    bedrooms = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username

    class Meta:
        managed = True
        db_table = 'details'
        verbose_name_plural = "details"
