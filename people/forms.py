from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    address = forms.CharField(max_length=300,
                              widget=forms.Textarea(
                                  attrs={'width': '100%', 'class': 'form-control'}
                              ))
    town_or_city = forms.CharField(max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'width': '100%', 'class': 'form-control'}
                                   ))
    county = forms.CharField(max_length=100,
                             widget=forms.TextInput(
                                 attrs={'width': '100%', 'class': 'form-control'}
                             ))
    postcode = forms.CharField(max_length=10,
                               widget=forms.TextInput(
                                   attrs={'width': '100%', 'class': 'form-control'}
                               ))
    number = forms.CharField(max_length=30,
                             help_text="Your contact telephone or mobile number",
                             widget=forms.TextInput(
                                 attrs={'width': '50%', 'class': 'form-control'}
                             ))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'width': '100%', 'class': 'form-control'}))
    rooms = forms.IntegerField(min_value=0,
                                        widget=forms.NumberInput(attrs={'width': '25%', 'class': 'form-control'}))
    floors = forms.IntegerField(min_value=0,
                                         widget=forms.NumberInput(attrs={'width': '25%', 'class': 'form-control'}))
    bathrooms = forms.IntegerField(min_value=0,
                                            widget=forms.NumberInput(attrs={'width': '25%', 'class': 'form-control'}))
    bedrooms = forms.IntegerField(min_value=0,
                                           widget=forms.NumberInput(attrs={'width': '25%', 'class': 'form-control'}))
    email_alerts = forms.BooleanField(
        help_text="Select to receive regular email updates on properties matching your searches",
        required=False
    )
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control'}))

    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control'}))


    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'password1', 'password2', 'username')
        widgets = {
            'first_name': forms.TextInput(attrs={'width': '100%', 'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'width': '100%', 'class': 'form-control'}),
            'username': forms.TextInput(attrs={'width': '100%', 'class': 'form-control'}),
        }
