from django.conf.urls import url

from . import views
from .views import Register

app_name = 'people'

urlpatterns = [
    url(r'^agent/(?P<user_id>[0-9]+)/$', views.agent_detail, name='agent_detail'),
    url(r'^owner/(?P<user_id>[0-9]+)/$', views.owner_detail, name='owner_detail'),
    url(r'^my_profile/$', views.my_profile, name='my_profile'),
    url(r'^my_preferences/$', views.my_preferences, name='my_preferences'),
    url(r'^tenant/(?P<user_id>[0-9]+)/$', views.tenant_detail, name='tenant_detail'),
    url(r'^owner/property_list/(?P<user_id>[0-9]+)/$', views.owner_property_list, name='owner_property_list'),
    url(r'^tenant/property_list/(?P<user_id>[0-9]+)/$', views.tenant_property_list, name='tenant_property_list'),
    url(r'^register/$', Register.as_view(), name='register'),
]
