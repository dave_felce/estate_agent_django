from rest_framework.test import APIRequestFactory, RequestsClient, APITestCase
from unittest import skip

@skip("SearchTests needs re-writing!")
class SearchTests(APITestCase):
    """ Test Search REST API """

    def setUp(self):
        self.client = RequestsClient()
        self.factory = APIRequestFactory()

    def test_property_search(self):
        query_data = {
            'amount_from': 0,
            'amount_to': 100000,
            'area': 'london hell town',
            'description': 'really cash',
            'for_rent': False
        }
        # Get properties as JSON from Search
        properties = rest_services.property_search(query_data)

        # properties may have more than 3 items as more are added..
        self.assertEqual(len(properties[0]), 20)
        self.assertEqual(len(properties[1]), 20)
        self.assertEqual(len(properties[2]), 20)
        self.assertEqual(properties[0]['postcode'], 'HA1MP5')
        self.assertEqual(properties[1]['postcode'], 'HE11')
        self.assertEqual(properties[2]['postcode'], 'BK5H16')
        self.assertEqual(properties[0]['name'], 'Mansion (The)')
        self.assertEqual(properties[1]['name'], 'The Pit')
        self.assertEqual(properties[2]['name'], 'The House')
