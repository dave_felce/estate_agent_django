from places.documents import PropertyDocument
from django.views.generic import ListView
from places.models import Property


class PropertySearch(ListView):

    model = Property

    def get_queryset(self):
        args = self.request.GET
        description = args.get('description', '')
        search = PropertyDocument.search().filter("term", description=description)
        qs = search.to_queryset()
        return qs
