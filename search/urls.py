from django.conf.urls import url

from .views import PropertySearch

app_name = 'search'

urlpatterns = [
    url(r'^search/$', PropertySearch.as_view(), name='search'),
    url(r'^rent/$', PropertySearch.as_view(), {'type': 'rent', 'template': 'rent'}, name='rent'),
    url(r'^sale/$', PropertySearch.as_view(), {'type': 'sale', 'template': 'sale'}, name='sale'),
]

