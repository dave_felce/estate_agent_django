import re
from django import forms
from django.core.validators import RegexValidator
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render
from urllib.parse import urlencode

from django.db.models import Max
from places.models import Property
# TODO: constants.py?

# pre-compiled regex to test form 'amount' against. Used in a couple of places in this module
amount_pattern = re.compile(r'^.{1}(\d+)\s[-]\s.{1}(\d+)$')
# The validator for the above regex
amount_validator = RegexValidator(amount_pattern, "Amount should be in the form £1 - £100")

class SimpleSearchForm(forms.Form):
    """Simple search form config, for validating against
    """

    def __init__(self, *args, **kwargs):
        super(SimpleSearchForm, self).__init__(*args, **kwargs)

    # From the amount slider bar (min and max values)
    amount = forms.CharField(label='Price range',
                             max_length=100,
                             required=True,
                             validators=[amount_validator])

    area = forms.CharField(label='Search area', max_length=255, required=True)
    description = forms.CharField(label='Property description', max_length=255, required=False)

    # List of possible choices tuples, for populating the radio buttons
    rent_or_sale_choices = [('rent', 'For rent'), ('sale', 'For sale')]
    rent_or_sale = forms.ChoiceField(choices=rent_or_sale_choices, widget=forms.RadioSelect(), required=True)

    def is_for_rent(self):
        """Utility method to decide if rental or sale property being passed

        Returns:
            boolean
        """
        return self.cleaned_data['rent_or_sale'] == 'rent'

    def get_money_range(self):
        """Process the text that comes in from the form, to get low and high range values

        Returns:
            low: (Str) of lowest range value
            high: (Str) of highest range value
        """
        match = re.match(amount_pattern, self.cleaned_data.get('amount'))
        low = match.group(1)
        high = match.group(2)
        return (low, high)

    def process_post(self, request, context):
        """Process the posted simple search form

        Params:
            request object,
            context, which must contain:
                page_title (page's title)
                reverse (originating page's view name, to return to in the event of errors or no results)
                template (originating page's template)

        Pre-processes form, returning to the originating page (rendering its template) with form errors
        if not valid, or if valid then passing on to further processing by search app (which will do the
        actual searching)

        This is all necessary so that originating pages can be displayed with errors, and successful GET
        requests to search can be bookmarked by users

        Returns:
            HttpResponse obj
        """

        # Return to originating page with the original context and form object for
        # displaying the form validation errors
        if not self.is_valid():
            context['form'] = self
            return render(request, context['template'], context)

        for_rent = self.is_for_rent()

        (amount_from, amount_to) = self.get_money_range()
        cleaned_data = {
            'amount_from': amount_from,
            'amount_to': amount_to,
            'area': self.cleaned_data.get('area'),
            'description': self.cleaned_data.get('description'),
            'for_rent': for_rent,
        }

        query_dict = cleaned_data.copy()
        query_dict.update({'reverse': context['reverse'], 'page_title': context['page_title']})
        query_str = urlencode(query_dict)
        search_url = reverse('search:search') + '?' + query_str  # TODO urljoin
        return HttpResponseRedirect(search_url)

class FullSearchForm(SimpleSearchForm):
    """Full search form config, for validating against. Inherits from the simple search form
    """

    def __init__(self, *args, **kwargs):
        super(FullSearchForm, self).__init__(*args, **kwargs)

        # Get the MAX values for each of these property attributes from the DB
        bathrooms_max = Property.objects.all().aggregate(Max('bathrooms'))['bathrooms__max'] or 0
        bedrooms_max = Property.objects.all().aggregate(Max('bedrooms'))['bedrooms__max'] or 0
        floors_max = Property.objects.all().aggregate(Max('floors'))['floors__max'] or 0
        rooms_max = Property.objects.all().aggregate(Max('rooms'))['rooms__max'] or 0
        bathrooms_choices = [(x,x) for x in range(1, bathrooms_max+1)]
        bedrooms_choices = [(x,x) for x in range(1, bedrooms_max+1)]
        floors_choices = [(x,x) for x in range(1, floors_max+1)]
        rooms_choices = [(x,x) for x in range(1, rooms_max+1)]

        self.fields['bathrooms'] = forms.ChoiceField(choices=bathrooms_choices, widget=forms.Select(), required=True)
        self.fields['bedrooms'] = forms.ChoiceField(choices=bedrooms_choices, widget=forms.Select(), required=True)
        self.fields['floors'] = forms.ChoiceField(choices=floors_choices, widget=forms.Select(), required=True)
        self.fields['rooms'] = forms.ChoiceField(choices=rooms_choices, widget=forms.Select(), required=True)

    def process_post(self, request, context):
        """Process the posted full search form

        Params:
            request object,
            context, which must contain:
                page_title (page's title)
                reverse (originating page's view name, to return to in the event of errors or no results)
                template (originating page's template)

        Pre-processes form, returning to the originating page (rendering its template) with form errors
        if not valid, or if valid then passing on to further processing by search app (which will do the
        actual searching)

        This is all necessary so that originating pages can be displayed with errors, and successful GET
        requests to search can be bookmarked by users

        Returns:
            HttpResponse obj
        """

        # Return to originating page with the original context and form object for
        # displaying the form validation errors
        if not self.is_valid():
            context['form'] = self
            return render(request, context['template'], context)

        # Boolean
        for_rent = self.is_for_rent()

        # Get amount range (low and high) from the slider input
        (amount_from, amount_to) = self.get_money_range()
        cleaned_data = {
            'amount_from': amount_from,
            'amount_to': amount_to,
            'area': self.cleaned_data.get('area'),
            'description': self.cleaned_data.get('description'),
            'for_rent': for_rent,
            'bathrooms': self.cleaned_data.get('bathrooms'),
            'bedrooms': self.cleaned_data.get('bedrooms'),
            'floors': self.cleaned_data.get('floors'),
            'rooms': self.cleaned_data.get('rooms'),
        }

        query_dict = cleaned_data.copy()
        query_dict.update({'reverse': context['reverse'], 'page_title': context['page_title']})
        query_str = urlencode(query_dict)
        search_url = reverse('search:process_request_full') + '?' + query_str
        return HttpResponseRedirect(search_url)



