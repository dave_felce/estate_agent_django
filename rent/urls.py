from django.conf.urls import url

from .views import Rent

app_name = 'rent'

urlpatterns = [
    url(r'^$', Rent.as_view(), name='index'),
]