from django.core.urlresolvers import reverse
from django.views import View
from django.shortcuts import render
from django.contrib.messages import get_messages
from places.models import Property
from search.forms import FullSearchForm
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

class Rent(View):

    def common_context(self):
        """Common values shared by get() and post() methods for this page
        """

        # 'reverse' is the URL for the rental index page: the origin of the form submission
        context = {
            'y': Property.objects.filter(for_rent=True).order_by('-updated')[:4],
            'page_title': 'For Rent',
            'reverse': reverse("rent:index"),
        }
        return context

    def get(self, request):
        """get method for page.  Initially, the form on this page will be empty
        """

        # First get common values
        context = self.common_context()
        # Update with the get() specific attributes
        context.update({
            'form': FullSearchForm(),
            'messages': get_messages(request),
        })
        return render(request, 'rent/index.html', context)

    def post(self, request):
        """post method for this page.

        Returns:
            HTTPResponse obj from form processing (search app)
        """

        # First get common attributes for this page
        context = self.common_context()
        # Update with the post() specific attributes
        context.update({
            'template': 'rent/index.html',
        })
        # Validate and/or submit form.  process_post() will display the original page's
        # template with the form errors if it fails - which is why the 'template' value is passed
        form = FullSearchForm(request.POST)
        return form.process_post(request, context)
