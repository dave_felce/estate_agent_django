from django.test import TestCase, Client
from model_mommy import mommy
from unittest import skip

class RentTests(TestCase):
    """ Test Rent """

    def test_for_rental_search(self):
        """ Rental search page

        TODO:
            This needs fleshing out, a lot!

        """
        response = self.client.get('/rent/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<title>For Rent</title>")

    @skip("RentTests needs re-writing!")
    def setUp(self):
        self.client = Client()
        user = mommy.make('User', username='HOME_TEST_PERSON')
        person = mommy.make('people.Details', user=user)
        mommy.make('places.Property', user=person)

    @skip("RentTests needs re-writing!")
    def test_index(self):
        response = self.client.get('/')

        self.assertEqual(response.status_code, 200)

        # pprint(vars(response))
        self.assertContains(response, '<title>EstateAgent</title>')
        self.assertContains(response, '<h3 class="page-header global-header">Latest properties</h3>')

