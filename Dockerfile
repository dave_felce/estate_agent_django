FROM python:3.6

RUN mkdir /project
WORKDIR /project

COPY requirements.txt /project/requirements.txt
RUN pip install -r requirements.txt

COPY . /project

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
