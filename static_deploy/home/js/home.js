
// var arr =  ['Home','Buy', 'Sell', 'Rent'] ; // This one you wont need you will get it from back-end

// for( i=0; i<arr.length; ++i) {
//     $('.david-nav-bar ').append(
//     "<li><a href=\"#\">"+arr[i]+"</a></li>");

// }

$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 100000, // Max property price from database
      values: [ 0, 2000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "£" + ui.values[ 0 ] + " - £" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "£" + $( "#slider-range" ).slider( "values", 0 ) +
      " - £" + $( "#slider-range" ).slider( "values", 1 ) );
  } );