from django.contrib.auth.forms import AuthenticationForm

class EstateagentAuthForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(EstateagentAuthForm, self).__init__(*args, **kwargs)

        self.base_fields['username'].widget.attrs['class'] = 'form-control'
        self.base_fields['username'].widget.attrs['placeholder'] = 'Username'

        self.base_fields['password'].widget.attrs['class'] = 'form-control'
        self.base_fields['password'].widget.attrs['placeholder'] = 'Password'