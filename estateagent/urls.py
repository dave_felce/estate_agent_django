"""estateagent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views
from django.conf import settings
from django.conf.urls.static import static
from .forms import EstateagentAuthForm


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^documents/', include('documents.urls')),
    url(r'^', include('home.urls', namespace="home")),
    url(r'^articles/', include('articles.urls', namespace="articles")),
    url(r'^people/', include('people.urls', namespace="people")),
    url(r'^places/', include('places.urls', namespace="places")),
    url(r'^rent/', include('rent.urls', namespace="rent")),
    url(r'^sale/', include('sale.urls', namespace="sale")),
    url(r'^search/', include('search.urls', namespace="search")),
    url(r'^accounts/login/?$', views.login, name='login',
        kwargs={
            'template_name': 'registration/login.html',
            'authentication_form': EstateagentAuthForm
        }
       ),
    url(r'^accounts/logout/$', views.logout, name='logout', kwargs={'next_page': '/'}),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
