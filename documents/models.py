from django.db import models
from places.models import Property
from people.models import Details

class Document(models.Model):
    id = models.AutoField(primary_key=True)
    property = models.ForeignKey(Property, on_delete=models.DO_NOTHING, )
    user = models.ForeignKey(Details, on_delete=models.DO_NOTHING, )
    notes = models.TextField()
    uri = models.TextField()

    class Meta:
        managed = True
        db_table = 'document'

class DocumentUserProperty(models.Model):
    Document = models.ForeignKey(Document, on_delete=models.DO_NOTHING, )
    user = models.ForeignKey(Details, on_delete=models.DO_NOTHING, )
    property = models.ForeignKey(Property, on_delete=models.DO_NOTHING, )

    class Meta:
        managed = True
        db_table = 'document_user_property'

