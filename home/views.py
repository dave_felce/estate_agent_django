from django.core.urlresolvers import reverse
from django.views import View
from django.shortcuts import render
from django.contrib.messages import get_messages
from places.models import Property
from search.forms import SimpleSearchForm
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

class Home(View):
    """Home page class-based view

    Args:
        View (:obj:Django View base class, required)

    """

    def common_context(self):
        """Common data for the context var: both get() and post() will
        require these
        """

        # latest_properties: last four updated rent or sale properties
        context = {
            'latest_properties': Property.objects.order_by('-updated')[:4],
            'page_title': 'Estate Agent',
            'reverse': reverse("home:index"),
        }
        return context

    def get(self, request):
        """get http method: search form will be empty
        """
        context = self.common_context()
        context.update({
            'form': SimpleSearchForm(),
            'messages': get_messages(request),
        })
        return render(request, 'home/index.html', context)

    def post(self, request):
        """post http method: search form will be submitted
        """
        form = SimpleSearchForm(request.POST)

        context = self.common_context()
        context.update({
            'template': 'home/index.html',
        })
        # Process the form and return the results
        return form.process_post(request, context)
