from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

class HomeTests(TestCase):

    def setUp(self):

        self.property = mommy.make('places.Property')
        self.user = self.property.user

    def test_index(self):

        url = reverse('home:index')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, '<title>Estate Agent</title>')
        self.assertContains(response, self.property)
