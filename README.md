# Estate Agent

## Setup

### Docker Compose Setup

```
docker-compose build
docker-compose up -d
docker-compose exec webapp python manage.py migrate
```

## Create ES index
```
docker-compose exec python manage.py search_index --create
```
## Tests

```
docker-compose exec webapp python manage.py test
```

They currently all fail.

## Logs

```
docker-compose logs webapp
```

## Fixtures
```
docker-compose exec webapp python manage.py loaddata fixtures/initial.json
```
Once you have loaded the fixtures issue this command to sync the ES instance

```
docker-compose exec webapp python manage.py search_index --rebuild
```

## Users

root:password1!   <-- Admin
fred:flintstone   <-- A normal user
