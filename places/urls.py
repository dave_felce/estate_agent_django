from django.conf.urls import url

from . import views

app_name = 'places'

urlpatterns = [
    url(
        r'^for_rental/detail/(?P<property_id>[0-9]+)/$',
        views.RentalDetail.as_view(),
        name='rental_detail'
    ),
    url(
        r'^for_sale/detail/(?P<property_id>[0-9]+)/$',
        views.SaleDetail.as_view(),
        name='sale_detail'
    ),
    url(
        r'^owned/detail/(?P<property_id>[0-9]+)/$',
        views.owned_detail,
        name='owned_detail'
    ),
    url(
        r'^rented/detail/(?P<property_id>[0-9]+)/$',
        views.rented_detail,
        name='rented_detail'
    ),
]
