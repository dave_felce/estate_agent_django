from django.db import models


from people.models import Details

class Property(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Details, on_delete=models.DO_NOTHING)
    for_rent = models.BooleanField() # TODO: Change this to an enum describing the type
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    postcode = models.CharField(max_length=10)
    description = models.TextField(max_length=5000)
    price = models.IntegerField()
    rooms = models.IntegerField()
    floors = models.IntegerField()
    bathrooms = models.IntegerField()
    bedrooms = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'property'
        verbose_name_plural = "properties"
