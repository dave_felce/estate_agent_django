from django_elasticsearch_dsl import DocType, Index
from .models import Property

PROPERTY = Index('properties')

@PROPERTY.doc_type
class PropertyDocument(DocType):
    class Meta:
        model = Property

        fields = [
            'name',
            'description',
            'for_rent',
            'address',
            'postcode',
            'price',
            'rooms',
            'bathrooms',
            'bedrooms',
        ]
