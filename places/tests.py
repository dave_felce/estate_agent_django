from django.conf import settings
from django.template.loader import render_to_string
from django.test import TransactionTestCase, Client
from django.urls import reverse

from model_mommy import mommy

class PlacesTests(TransactionTestCase):
    """Test Places (properties)"""

    def setUp(self):

        self.place = mommy.make('places.Property')

    def test_owned_property_context(self):

        url = reverse('places:sale_detail', kwargs={'property_id': self.place.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.place.name, response.context['property'].name)
        self.assertEqual(self.place.description, response.context['property'].description)

    def test_owned_property_template(self):

        url = reverse('places:sale_detail', kwargs={'property_id': self.place.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Owned detail</title>')
        self.assertContains(response, f'<p>{self.place.description}</p>')
        self.assertContains(response, '<div class="page-content"')

    def test_rented_property_context(self):

        url = reverse('places:rental_detail', kwargs={'property_id': self.place.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.place.name, response.context['property'].name)
        self.assertEqual(self.place.description, response.context['property'].description)

    def test_rented_property_template(self):

        url = reverse('places:rental_detail', kwargs={'property_id': self.place.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, f'<title>Rental detail</title>')
        self.assertContains(response, f'<p>{self.place.description}</p>')
        self.assertContains(response, '<div class="page-content">')
