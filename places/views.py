from abc import ABCMeta, abstractmethod
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render
from django.views import View

from .models import Property
from people.models import Details
from search.forms import SimpleSearchForm

class PropertyDetail(View, metaclass=ABCMeta):
    """Detail for a single for_rental property.  Base class for RentalDetail and SaleDetail
    """

    @abstractmethod
    def get(self, request, property_id):
        """get form method: sets up common attributes, but must be overridden in child class
        """
        property = get_object_or_404(Property, pk=property_id)
        person = get_object_or_404(Details, pk=property.user_id)
        self.context = {
            'form': SimpleSearchForm(),
            'property': property,
            'person': person,
        }

    @abstractmethod
    def post(self, request, property_id):
        """post form method: sets up common attributes, but must be overridden in child class
        """
        self.form = SimpleSearchForm(request.POST)
        property = get_object_or_404(Property, pk=property_id)
        person = get_object_or_404(Details, pk=property.user_id)
        self.context = {
            'person': person,
            'property': property,
        }

class RentalDetail(PropertyDetail):
    """Detail for a single for_rent property. Inherits from PropertyDetail base class
    """

    def get(self, request, property_id):
        """get form method: overrides get() in base class, which will already have set up some common
        values for context var.  Add in the specific ones needed by this action
        """

        super().get(request, property_id)
        self.context.update({
            'page_title': 'Rental detail',
            'reverse': reverse("places:rental_detail", kwargs={'property_id': property_id})
        })
        return render(request, 'places/for_rental/detail.html', self.context)

    def post(self, request, property_id):
        """post form method: overrides post() in base class, which will already have set up some common
        values for context var.  Add in the specific ones needed by this action
        """

        super().post(request, property_id)
        self.context.update({
            'page_title': 'Rental detail',
            'template': 'places/for_rental/detail.html',
            'reverse': reverse("places:rental_detail", kwargs={'property_id': property_id}),
        })
        return self.form.process_post(request, self.context)

class SaleDetail(PropertyDetail):
    """Detail for a single for_sale property. Inherits from PropertyDetail base class
    """
    def get(self, request, property_id):
        """get form method: overrides get() in base class, which will already have set up some common
        values for context var.  Add in the specific ones needed by this action
        """

        super().get(request, property_id)
        self.context.update({
            'page_title': 'Sale detail',
            'reverse': reverse("places:sale_detail", kwargs={'property_id': property_id})
        })
        return render(request, 'places/for_sale/detail.html', self.context)

    def post(self, request, property_id):
        """post form method: overrides post() in base class, which will already have set up some common
        values for context var.  Add in the specific ones needed by this action
        """

        super().post(request, property_id)
        self.context.update({
            'page_title': 'Sale detail',
            'template': 'places/for_sale/detail.html',
            'reverse': reverse("places:sale_detail", kwargs={'property_id': property_id}),
        })
        return self.form.process_post(request, self.context)

def owned_detail(request, property_id):
    """Detail for a single property that is owned.  By the logged in user, or a user_id?  Not sure at this point

    TODO: sort out the above question..
    """

    property = get_object_or_404(Property, pk=property_id)
    person = get_object_or_404(Details, pk=property.user_id)
    context = {'property': property, 'person': person, 'page_title': 'Owned detail'}
    return render(request, 'places/owned/detail.html', context)

def rented_detail(request, property_id):
    """Detail for a single property that is owned.  By the logged in user, or a user_id?  Not sure at this point

    TODO: sort out the above question..
    TODO: Can some of these methods and templates be replace by filters, as they are almost identical?
    """

    property = get_object_or_404(Property, pk=property_id)
    person = get_object_or_404(Details, pk=property.user_id)
    context = {'property': property, 'person': person, 'page_title': 'Rented detail'}
    return render(request, 'places/rented/detail.html', context)
