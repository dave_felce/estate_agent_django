from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.inclusion_tag('places/latest_properties.html', takes_context=True, name='latest_properties')
def latest_properties(context, **kwargs):
    """The latest properties display at the top of home, rent and buy index pages
    """

    return {
        'latest_properties': context['latest_properties'],
    }

@register.inclusion_tag('places/card_content.html')
def x(queryset):
    """The concatenated and trimmed text for the cards, to keep the size consistent

    TODO: Should be in card-group
    """

    place = queryset.get()

    card_content = ', '.join([
            place.description,
            place.postcode,
        ]
    )

    card_content = (card_content[:40] + '..') if len(card_content) > 40 else card_content

    return {
        'card_content': mark_safe(card_content),
        'latest_properties': place,
    }

